#/usr/bin/env python
'''
cliargs

The command line arguments.
'''
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--debug',
                    help="Log the debug information to stdout.",
                    action='store_true')
parser.add_argument('--version',
                    help='print the version of report.py and then exit.',
                    action='store_true')
parser.add_argument('-u', '--update-expected',
                    help='Update the expected grades assignment on Canvas',
                    action='store_true')
args = parser.parse_args()
