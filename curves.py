"""
curves

Computes the curves for each assignment group and
for each student's overall expected grade.
"""
from fractions import Fraction

import customvalues

cv = customvalues  # lazy to type full name throughout program

def gradefor(score, bounds):
    """Converts score to letter grade, looking up in a table of lower bounds
    """
    for letter_grade, lower_bound in bounds:
        if score >= lower_bound :
            return letter_grade
    return 'F'

def lab_curves(students, num_labs_so_far):
    '''Calculates the lab curves for each Student and adds the
    weighted grade to the `group_curves` dictionary.
    '''

    for student in students:
        lab_total_score = student.group_grade_totals['lab']
        if num_labs_so_far > 0 :
            score = lab_total_score * cv.total_labs / num_labs_so_far
            student.group_curves['lab'] = cv.letter_grade_to_score[gradefor(score, cv.lab_boundaries)]
        else:
            student.group_curves['lab'] = -1

def grade_curves(students, group_name, percentages):
    '''Given a list of students with homework or quiz grades
    ordered from highest-to-lowest and an ordered list of percentages for each grade
    add the curved grade for each student.group_curves'''

    # sort grades in decreasing order by the assignment group totals
    sorted_students = sorted(students,
        key=lambda s: s.group_grade_totals[group_name], reverse=True)

    letter_grade_order = cutoffs(len(students), percentages)

    counter = 0
    for letter, num_for_grade in letter_grade_order.items():
        for i in xrange(num_for_grade):
            student = sorted_students[counter]
            score = student.group_grade_totals[group_name]
            if score == 0: # Can't get better than an F for 0 points
                student.group_curves[group_name] = 0
            elif counter > 0 and score == sorted_students[counter - 1].group_grade_totals[group_name]:
                # If score identical to precedessor, can't give lower grade
                student.group_curves[group_name] = sorted_students[counter - 1].group_curves[group_name]
            else :
                # Give score for rank
                student.group_curves[group_name] = cv.letter_grade_to_score[letter]
            counter += 1

def exam_totals(students, agroups):
    '''Calculates the exam totals for each Student and adds the
    weighted grade the `group_curves` dictionary.

    The exam totals are considered as raw points and are not curved.
    '''
    exam_group = next((ag for ag in agroups \
                       if ag.name == cv.group_names['exam']))
    exams = exam_group.assignments
    exams_points_possible = [e.points_possible for e in exams]
    exams_total_possible = sum(exams_points_possible)

    if exams_total_possible == 0:  # No exams yet
        for s in students:
            s.group_curves['exam'] = -1

        return

    a_grade = cv.letter_grade_to_score['A+']
    for s in students:
        exam_total = s.group_grade_totals['exam']
        grade = (float(exam_total) / exams_total_possible) * a_grade
        s.group_curves['exam'] = grade

def cutoffs(num_students, percentages):
    '''Returns the number of students in each range.'''

    result = percentages.copy()
    denom = sum(percentages.values()) # Should be 100, but just in case...
    # round number of students per grade up to the nearest integer
    for letter_grade, cutoff in percentages.items():
        result[letter_grade] = int(num_students * cutoff / denom + 0.5)

    # 'F' students are the rest of them
    result['F'] = 0
    sum_abcd = sum(result.values())
    result['F'] = num_students - sum_abcd

    return result

def curve_grades(students, agroups, num_labs_so_far):
    '''Calculates the curves for all of the assignment groups
    for each Student.
    '''
    lab_curves(students, num_labs_so_far)

    grade_curves(students, 'quiz', cv.quiz_percentages)
    grade_curves(students, 'hw', cv.hw_percentages)
    exam_totals(students, agroups)

def group_grade_totals(students):
    '''Aggregates all the grades for each assignment
    group and saves it in each Student'''

    def sum_section(student, assignment_group):
        """Sums the grades for the assignment group of the student."""
        # The assignment group's graded assignments for this student
        group_subs = student.graded_submissions[cv.group_names[assignment_group]]

        # All the grades from the above list of assignments
        student_pts_in_group = [s.points_received for s in group_subs]

        # the sum of all the above grades
        return sum(student_pts_in_group)

    for student in students:
        # get the grade totals for each group for this student
        hw = sum_section(student, 'hw')
        quiz = sum_section(student, 'quiz')
        exam = sum_section(student, 'exam')
        lab = sum_section(student, 'lab')

        # save the grade totals into the student field `group_grade_total`
        student.group_grade_totals['hw'] = hw
        student.group_grade_totals['quiz'] = quiz
        student.group_grade_totals['exam'] = exam
        student.group_grade_totals['lab'] = lab

def expected_grades(students):
    """Calculates the expected grades for each student and
    saves the expected grades into each student's expected_grade
    """
    for s in students:
        scores = 0
        weights = 0
        for group_name, weight in cv.group_weights.items():
            score = s.group_curves[group_name]
            if score >= 0 : # -1 used for invalid score
                scores += score * weight
                weights += weight
        if weights > 0 :
            scores = scores / weights
            
        s.expected_grade = (scores, gradefor(scores, cv.grade_lower_boundaries))
