#!/usr/bin/env python

"""
report

Creates progress reports for students
"""
import sys
import datetime
import dateutil.parser
from dateutil.tz import tzlocal
import logging
from collections import defaultdict

from canvas.assignmentgroup import AssignmentGroup
from canvas.assignment import Assignment
from canvas.student import Student
from canvas.submission import Submission
import canvasrequests.configrequests

import curves
import customvalues
import reportcsv

__author__ = "Daniel Mai"
__version__ = "2014-03-20"

# alias to simply shorten name of customvalues
cv = customvalues

# Mapping of Assignment -> a list of at-risk Students
missed_assignment_risk = defaultdict(list)

def get_students():
    """Returns a list of Student objects.

    Each student has a name,
                       Canvas ID,
                       a dictionary of graded submissions
                       a dictionary of total points per group grade
                       a tuple that represents the expected grade in the form
                           (point, letter)

    """
    def get_students_from_canvas():
        """Gets the JSON list of students from Canvas."""
        # Request for the students in the course from Canvas
        r = cr.list_users_in_course(enrollment_role='StudentEnrollment',
                                    include='email')
        logger.debug("Pagination links: %s", r.links.get('next'))

        # The JSON list of students from Canvas
        students = r.json()

        # Canvas paginates the results of large JSON requests to a maximum of 50
        # items, so this loop will get all of the students by going through all of
        # the paginated results.
        while r.links.get('next'):
            r = cr.get(r.links['next']['url'])
            students += r.json()
        return students

    # The `result` list will hold the Student objects for the course
    result = []

    # Go through all students in the course
    for student in get_students_from_canvas():

        # This is a stub dictionary for the AssignmentGroup objects that each
        # student will have for their graded submissions. It will be updated
        # later in the `gather_assignment_submissions` method.
        graded_sub_stubs = { gname:[] for gname in cv.group_names.values() }

        # These are the fields to initialize for each Student object that will
        # represent every student in the course in the script
        kwargs = {
            'name': student.get('name'),
            'sortable_name': student.get('sortable_name'),
            'canvas_id': student.get('id'),
            'email': student.get('email'),
            'graded_submissions': graded_sub_stubs,
            'group_grade_totals': {},
            'group_curves': {},
            'expected_grade': ()
        }

        # Put this student in the `result` list as a Student object with the
        # above fields instantiated
        result.append(Student(**kwargs))
    return result

def get_assignment_groups():
    """Gets the name, canvas id, weight, and assignments
    for each assignment group"""
    # Depending on the settings found in customvalues.py, either get assignment
    # groups by the Assignment Groups already set in Canvas, ...
    if cv.grade_by_assignment_group:
        return get_assignment_groups_by_group()

    # ... or get them just by looking at a common prefix between assignments.
    return get_assignment_groups_by_name()


def get_assignment_groups_by_group():
    """Gets the assignment groups based on the Assignment Groups
    already on Canvas."""
    # Request for the assignment groups from Canvas
    r = cr.list_assignment_groups()

    # The JSON list of assignment groups
    agroups = r.json()

    # The `result` list will hold all of the AssignmentGroup objects to return
    result = []

    # Iterate through each assignment group in the JSON retrieved from Canvas
    for agroup in agroups:

        # Make sure the assignment group is one that we want to consider for
        # grading. i.e., if it's in the group names defined in customvalues.py
        if agroup['name'] in cv.group_names.values():

            # Here, we get the name, canvas id, and the weight for
            # each assignment group
            agroup_info = {
                'name': agroup['name'],
                'canvas_id': agroup['id'],
#                'weight': cv.group_weights[agroup['name']]
                }

            # This is the http request to get the assignment groups
            # with the Canvas API
            r = cr.get_an_assignment_group(agroup_info['canvas_id'],
                                           'assignments')

            # Get the JSON for just the assignments
            r_assignments = r.json()['assignments']

            # Save the assignments as AssignmentGroup objects
            assignments = []
            for assignment in r_assignments:
                # The due date for this assignment
                due_date = assignment['due_at']

                # If the due date is not None (if there is a due date
                # set on Canvas, create a datetime object of the due
                # date so we can use it for checking the dates later
                if due_date:
                    due_date = dateutil.parser.parse(due_date)
                    due_date = due_date.astimezone(tzlocal())

                points_possible = assignment['points_possible']

                # If there's no points set yet, set the points possible to 0
                if points_possible is None:
                    points_possible = 0

                # In the case of the assignment being graded by letter grades,
                if assignment['grading_type'] == 'letter_grade':
                    points_possible = cv.letter_grade_to_score["A+"]

                # Here, we set the name, canvas id, the maximum points
                # possible, and the due date fields for an assignment.
                kwargs = {
                    'name': assignment['name'],
                    'canvas_id': assignment['id'],
                    'points_possible': points_possible,
                    'due_date': due_date
                }

                # If this assignment is NOT in the assignment ignore
                # list (found in customvalues.py), then save the
                # assignment for grading
                if assignment['name'] not in cv.assignment_ignore_list:
                    assignments.append(Assignment(**kwargs))

            # Add the list of assignments to the assignment group information
            agroup_info['assignments'] = assignments

            # Add an assignment with the assignment group's key-value
            # pairs as arguments for an AssignmentGroup and add it to
            # the result list
            result.append(AssignmentGroup(**agroup_info))
    return result

def get_assignment_groups_by_name():
    """Gets the assignment groups based on the prefixes of the
    assignment names. For example, if the homework assignments are called

    Homework 1 Draft
    Homework 1 Final
    Homework 2 Draft
    Homework 2 Final

    then the prefix that matches them will be "Homework".
    """

    # Get the list of assignments by calling `list_assignments` from
    # the Canvas API
    r = cr.list_assignments()

    # The assignment groups
    agroups = cv.group_names.values()

    result = []
    for agroup in agroups:
        d = {}
        d['name'] = agroup
        assignments = []
        for assignment in r.json():
            if (assignment['name'].lower().startswith(agroup.lower()) and
                assignment['name'] not in cv.assignment_ignore_list):
                due_date = assignment['due_at']
                if due_date:
                    due_date = dateutil.parser.parse(due_date)
                    duedate = due_date.astimezone(tzlocal())
                kwargs = {
                    'name': assignment['name'],
                    'canvas_id': assignment['id'],
                    'points_possible': assignment['points_possible'],
                    'due_date': due_date
                }
                assignments.append(Assignment(**kwargs))
        d['assignments'] = assignments
#        d['weight'] = cv.group_weights[agroup]
        result.append(AssignmentGroup(**d))
    return result

def get_student_submission(assignment, submissions, student):
    """Gets the submission of the student within submissions."""

    def numbered_score(grade):
        """Converts the grade (as as string) to a numbered score.
        Converts letter grades to a numbered score too.
        """
        if grade is None:
            grade = 0
        try:
            score = float(grade)
        except ValueError:
            # At this point, the grade is assumed to be a letter grade
            score = cv.letter_grade_to_score[grade]
        return score

    # The Canvas ID
    assignment_id = assignment.canvas_id

    # The student's submission
    submission = next((s for s in submissions \
              if s['user_id'] == student.canvas_id), None)

    # If there is actually a submission, then create a Submission out
    # of it with their score; else, create a Submission with a 0 score.
    if submission:
        grade = submission['grade']
        score = numbered_score(grade)
        return Submission(assignment_id, score, submission['submitted_at'])
    else:
        return Submission(assignment_id, 0, None)

def get_last_two_labs(num_students, lab_group):
    """Gets the lab assignments for the last two weeks
    Criteria for a valid lab is if at least 50% of the
    class submitted reports for it.

    """
    num_half_class = float(num_students) / 2
    logger.info('Getting two most recent labs.')

    # check for the first undone lab and return (lab1, lab2)

    # start with the initial lab being the first lab ever
    try:
        lab1 = lab2 = lab_group.assignments[0]
    except IndexError:
        logger.info("There are no labs.")
        return None

    num_labs_so_far = 0
    for index, lab_assignment in enumerate(lab_group.assignments):
        num_labs_so_far = index
        logger.info("Checking '%s'", lab_assignment.name)
        now = datetime.datetime.now(tzlocal())

        # If the lab has a due date and it hasn't even passed yet, stop the loop
        if lab_assignment.due_date and not lab_assignment.due_before(now):
            break
        r = cr.list_assignment_submissions(lab_assignment.canvas_id)

        # a student submission has a submission time
        submissions = [s for s in r.json() if s['submitted_at'] is not None]

        num_submissions = len(submissions)
        logger.debug('number of submissions = %d', num_submissions)

        # If the number of submissions is less than 50% of the class, then break
        if num_submissions < num_half_class:
            break
        lab1, lab2 = lab2, lab_assignment
    return (lab1, lab2, num_labs_so_far)

def gather_assignment_submissions(agroups, students):
    """Gets the assignment submissions for each assignment
    and saves them to each Student. Also takes note of the at-risk
    students if they missed an assignment that was due within
    the last two weeks.
    """
    # Get the current date and the date exactly two weeks agoto determine which
    # assignments were due within the past two weeks for the at-risk-student
    # calculation.
    now = datetime.datetime.now(tzlocal())
    two_weeks_ago = now - datetime.timedelta(weeks=2)

    # Iterate through each AssignmentGroup and grab the assignment submissions
    for agroup in agroups:
        logger.info("Getting assignments in group '%s'", agroup.name)

        # Iterate through each Assignment for each AssignmentGroup
        for assignment in agroup.assignments:
            logger.info('Getting grades for %s' % assignment.name)

            # Request for the assignment submissions from Canvas
            r = cr.list_assignment_submissions(assignment.canvas_id)

            # The JSON list of assignment submissions
            r_assignment_subs = r.json()

            # For this assignment, go though all of the students in the class
            # and get their submission if they have one.
            for student in students:
                # This student's submission for this assignment
                student_sub = get_student_submission(assignment,
                                                     r_assignment_subs, student)

                # check for at-risk students for assignments within 2 weeks. A
                # potential at-risk student is determined by checking if they
                # did not have a score or received of zero (anything that
                # resolves to the boolean `False` value in Python.
                if not student_sub.points_received:

                    # If the assignment that the student got a zero on was due
                    # within the past two weeks or if the assignment is part of
                    # the two most recent labs that were due recently, then this
                    # student will be added to the list of at-risk students for
                    # this assignment.
                    if (assignment.due_within(two_weeks_ago, now) or
                        two_recent_labs and assignment in two_recent_labs):
                        missed_assignment_risk[assignment].append(student)

                # Add this graded submission to the list of other assignments
                # that this student has done.
                student.graded_submissions[agroup.name].append(student_sub)

    # Show what's in the AssignmentGroups
    logger.debug(repr(agroups))

def update_expected_grades_on_canvas(students):
    """
    Updates the 'expected grades' section on Canvas for
    each student.
    """

    assignment_name = cv.expected_grades_assignment_name
    assignment_id = cr.get_assignment_id(assignment_name)

    for student in students:
        grade = student.expected_grade[1]  # the letter grade
        name = student.name
        student_id = student.canvas_id

    input = raw_input("Ready to update grades? (y/n) ")
    if input.lower() == 'y':
        message = "Uploading '%s' grade for %s (%s)"
        for student in students:
            grade = student.expected_grade[1]
            name = student.sortable_name
            student_id = student.canvas_id
            logger.info(message, grade, name, student_id)
            cr.grade_or_comment_on_a_submission(assignment_id, student_id,
                                                grade=grade)

if __name__ == "__main__":
    import args.cliargs

    if args.cliargs.args.version:
        print 'report.py version:', __version__
        sys.exit()

    cr = canvasrequests.configrequests.courserequests

    # set up logging
    ## Set up logging for this module
    logger = logging.getLogger(__name__)
    logfmt = '%(asctime)s %(levelname)s: %(message)s'
    loglevel = logging.DEBUG if args.cliargs.args.debug else logging.INFO
    logging.basicConfig(format=logfmt,level=loglevel)

    ## Override norrmal logging calls from requests library
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.WARNING)

    logger.debug(cv.assignment_groups)
    logger.debug(cv.group_names)

    logger.info("Getting student information from Canvas.")
    students = get_students()
    logger.debug('number of students = {ns}'.format(ns=len(students)))

    logger.info("Getting Canvas assignment groups.")
    agroups = get_assignment_groups()
    # check for two most recent labs
    lab_group = next((agroup for agroup in agroups \
                      if agroup.name == cv.group_names['lab']))

    two_recent_labs = get_last_two_labs(len(students), lab_group)
    num_labs_so_far = two_recent_labs[2]

    # log the correct message according whether or not 'two_recent_labs' has two
    # unique labs or not
    if two_recent_labs[0] == two_recent_labs[1]:
        logger.info("The only recent lab is '%s'.", two_recent_labs[0].name)
    else:
        logger.info("The two most recent labs are '%s' and '%s'.",
                    two_recent_labs[0].name,
                    two_recent_labs[1].name)

    logger.info("number of labs so far = %d", num_labs_so_far)

    logger.info("Getting assignment submissions per group.")
    gather_assignment_submissions(agroups, students)

    curves.group_grade_totals(students)
    curves.curve_grades(students, agroups, num_labs_so_far)
    curves.expected_grades(students)
    logger.info("Creating the report.")
    reportcsv.create_report(students, missed_assignment_risk)

    if args.cliargs.args.update_expected:
        logger.info("Updating expected grades.")
        update_expected_grades_on_canvas(students)
