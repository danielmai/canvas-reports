class Submission(object):


    def __init__(self, assignment_id, points_received=0, submit_time=None):
        self.assignment_id = assignment_id
        self.points_received = points_received
        self.submit_time = submit_time

    def __repr__(self):
        return 'Submission({ai},{pr},{st})'.format(
            ai=repr(self.assignment_id),
            pr=repr(self.points_received),
            st=repr(self.submit_time)
        )
