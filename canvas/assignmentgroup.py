class AssignmentGroup(object):


    def __init__(self, name=None, canvas_id=None, weight=None, assignments=[]):
        self.name = name   
        self.canvas_id = canvas_id
        self.weight = weight
        self.assignments = assignments
        
    def __repr__(self):
        return ('AssignmentGroup({name},{canvas_id},{weight},'
                '{assignments})').format(name=repr(self.name),
                                        canvas_id=repr(self.canvas_id),
                                        weight=repr(self.weight),
                                        assignments=repr(self.assignments))
