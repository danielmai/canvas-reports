class Assignment(object):

    def __init__(self, name, canvas_id, points_possible, due_date):
        """Initialize an Assignment.

        name: this Assignment's name
        canvas_id: the Canvas ID for this Assignment
        points_possible:

        """
        self.name = name
        self.canvas_id = canvas_id
        self.points_possible = points_possible
        self.due_date = due_date

    def due_before(self, bound_time):
        """If this assignment as a due_date, due_before
        checks if this assignment is due before bound_time.

        bound_time is a naive datetime object.

        Return True if this assignment is due before time.
        False otherwise.

        """
        if self.due_date:
            return self.due_date < bound_time
        return False

    def due_within(self, lbound_time, ubound_time):
        """If this assignment has a due_date, due_within
        checks if this assignment is due within
        lbound_time and ubound_time. lbound_time is the
        lower bound check, and ubound_time is the upper
        bound check.

        lbound_time and ubound_time are datetime objects.

        Return True if this assignment due date is within
        the two times. False otherwise.

        """
        if self.due_date:
            return lbound_time <= self.due_date <= ubound_time
        return False


    def __repr__(self):
        return 'Assignment({name},{cid},{pp},{dd})'.format(
            name=repr(self.name),
            cid=repr(self.canvas_id),
            pp=repr(self.points_possible),
            dd=repr(self.due_date)
        )
