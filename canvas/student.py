class Student(object):


    def __init__(self, name, sortable_name, canvas_id, email,
                 graded_submissions={}, group_grade_totals={},
                 group_curves={}, expected_grade=0, rank=0):
        self.name = name
        self.sortable_name = sortable_name
        self.canvas_id = canvas_id
        self.email = email
        self.graded_submissions = graded_submissions
        self.group_grade_totals = group_grade_totals
        self.group_curves = group_curves
        self.expected_grade = expected_grade
        self.rank = 0

    def __str__(self):
        return '"{sortable_name}",{canvas_id},{email}'.format(
            sortable_name=self.sortable_name,
            canvas_id=self.canvas_id,
            email=self.email
        )

    def __repr__(self):
        return ('Student({name},{sname},{cid},'
                '{email},{gs},{ggt},{gc},{eg},{r})').format(
                    name=repr(self.name),
                    sname=repr(self.sortable_name),
                    cid=repr(self.canvas_id),
                    email=repr(self.email),
                    gs=repr(self.graded_submissions),
                    ggt=repr(self.group_grade_totals),
                    gc=repr(self.group_curves),
                    eg=repr(self.expected_grade),
                    r=repr(self.rank)
                )
