"""
reportcsv

Creates the csv report.
"""
import csv
import datetime
import itertools
import logging
import math
import operator
import os
import sys

import customvalues
import curves

cv = customvalues
table_separator = [""]  # an empty row

pathname = os.path.dirname(sys.argv[0])
# absolute path to this file
report_dir = os.path.abspath(pathname)

def assign_rank(students):
    ranked_students = sorted(students, key=lambda x: x.expected_grade[0],
                             reverse=True)
    for rank, student in enumerate(ranked_students, start=1):
        student.rank = rank

def lowest_twenty_percent(students):
    """Given a list of students, return the list students
    whose expected grades are in the lowest 20% range.
    """
    cutoff = 0.20  # least twenty percent
    num_lowest_twenty_percent = int(math.ceil(len(students) * cutoff))

    # sort students by least-to-greatest expected grades
    sorted_students = sorted(students, key=lambda s: s.expected_grade)

    # return the bottom 20% of students -- these are the at-risk students
    return [sorted_students[i] for i in xrange(num_lowest_twenty_percent)]


def create_report(students, missed_assignment_risk):
    # Set up logger for this method
    logger = logging.getLogger('reportcsv')

    # Get the date and time right now
    now = datetime.datetime.now()

    # format the date as 'YEAR-MONTH-DAY HOURS:MINUTES:SECONDS AM/PM' to use
    # in the report header
    date_now_full = now.strftime("%Y-%m-%d %I:%M:%S %p")

    # format the date as 'YEAR-MONTH-DAYTHOURS:MINUTES:SECONDS' to use in the
    # filename
    date_now_filename = now.strftime("%Y-%m-%dT%H-%M-%S")

    # Give all of the students a rank based on their expected grade points
    assign_rank(students)

    # This is the `csv-reports` directory that will hold all of the csv report
    # files
    csv_report_dir = "{}/csv-reports/".format(report_dir)

    # create directory 'csv-reports/' if it doesn't exist
    try:
        os.stat(csv_report_dir)
    except OSError:
        # The directory doesn't exist, so create it.
        os.makedirs(csv_report_dir)
        logger.info("Created directory %s", csv_report_dir)

    # The file name for this report file
    report_filename = 'report-{}.csv'.format(date_now_filename)

    # The full path to this report file
    report_filepath = '{}/{}'.format(csv_report_dir, report_filename)

    # Create a file for the report and write to it as a csv file.
    with open(report_filepath, 'w') as reportfile:
        # The writer for the csv file
        csv_report = csv.writer(reportfile, quoting=csv.QUOTE_MINIMAL)

        # Write the time this report was created
        csv_report.writerow(['Report produced at %s' % date_now_full])

        # Write the course name associated with this report
        csv_report.writerow(['Course: %s' % cv.course_name])

        csv_report.writerow(table_separator)

        # Write the at-risk report to the report file
        csv_report.writerows(create_at_risk_report(missed_assignment_risk))
        csv_report.writerow(table_separator)

        # Write the lowest 20% reprot to the report file
        csv_report.writerows(create_lowest_20_report(
                                    lowest_twenty_percent(students)))
        csv_report.writerow(table_separator)

        # Write the whole class report to the report file
        csv_report.writerows(create_whole_class_report(students))

    # Inform where the report file was written to
    logger.info("Report file '%s' created in '%s'",
                report_filename, csv_report_dir)

def create_at_risk_report(missed_assignment_risk):
    """Creates the report for the at-risk students.

    Returns a list of all the rows needed to write the csv table."""

    # The description for this report
    description = [('Students with a zero-score on an assignment(s) from '
                    'the past two weeks')]

    # The headers for this table
    header_row = ['Name','Canvas ID','Email','Expected Grade','Class Rank']

    # Add the assignment names that students have missed within the past two
    # weeks as other headers with the ones above
    assignment_headers = [a.name for a in sorted(missed_assignment_risk,
                                         key=operator.attrgetter('name'))]

    # Connect the header_row and assignment_headers into one row of headers
    header_full = list(itertools.chain(header_row,assignment_headers))

    # students is a list of every student that has missed an assignment. It
    # may contain duplicates
    students = [s for ss in missed_assignment_risk.values() for s in ss]

    # Create a set out of the students to get rid of the students
    risk_students = set(students)

    # Gather a row for each student, in alphabetical order by last name.
    studentrows = []
    for student in sorted(risk_students,
                          key=operator.attrgetter('sortable_name')):
        # These are the elements that correspond to the initial header
        # columns (without the assignment names)
        student_info_row = [student.sortable_name,
                           student.canvas_id,
                           student.email,
                           student.expected_grade[1], # letter
                           student.rank]

        # Go through all the assignments that any student has missed, and
        # mark the ones that this particular student has missed with an 'x'.
        # Start off with a list the same length as the number of assignments
        # that have been missed with empty strings.
        assignment_row = [''] * len(assignment_headers)
        for i, assignment in enumerate(assignment_headers):
            # Get the Assignment object with the matching assignment name,
            # which is used as the key in the `missed_assignment_risk`
            # dictionary.
            a_obj = next(a for a in missed_assignment_risk \
                         if a.name == assignment)


            if student in missed_assignment_risk[a_obj]:
                assignment_row[i] = 'x'

        # Connect the two rows together to create one row
        row = list(itertools.chain(student_info_row, assignment_row))

        # Add this row to the rows of students to write later
        studentrows.append(row)

    # Return a list of all the rows for this table -- the description, header
    # rows, and the rows with the students
    return list(itertools.chain([description, header_full], studentrows))

def create_lowest_20_report(low_20):
    """Create the report for the lowest 20% of students.

    Returns a list of all the rows needed to write the csv table."""

    # The description for this report
    description = ["Lowest 20% of students"]

    # The header row for this table
    header_row = ['Name',
                  'Canvas ID',
                  'Email',
                  'Expected Grade',
                  'Class Rank']

    # Every row for each student will contain an entry that corresponds to each
    # header in the above list.
    studentrows = []
    for student in low_20:
        studentrows.append([student.sortable_name,
                            student.canvas_id,
                            student.email,
                            student.expected_grade[1], # letter
                            student.rank])

    # Return a list of all the rows for this table -- the description, header
    # rows, and the rows with the students
    return list(itertools.chain([description, header_row], studentrows))

def create_whole_class_report(students):
    """Creates the report for the lowest 20% of students.

    Returns a list of all the rows needed to write the csv table."""

    # The description for this table
    description = ["Class progress report"]

    # The headers for this table
    header_row = ['Name',
                  'Canvas ID',
                  'Email',
                  'Homework Total',
                  'Quiz Total',
                  'Lab Total',
                  'Exam Total',
                  'Homework Grade',
                  'Quiz Grade',
                  'Lab Grade',
                  'Exam Grade',
                  'Weighted Average',
                  'Expected Grade',
                  'Class Rank']

    # Every row for each student will contain an entry that corresponds to each
    # header in the above list
    studentrows = []
    for student in students:
        row = [student.sortable_name,
               student.canvas_id,
               student.email,
               student.group_grade_totals['hw'],
               student.group_grade_totals['quiz'],
               student.group_grade_totals['lab'],
               student.group_grade_totals['exam'],
               student.group_curves['hw'],
               student.group_curves['quiz'],
               student.group_curves['lab'],
               student.group_curves['exam'],
               student.expected_grade[0], # weighted average
               student.expected_grade[1], # letter grade
               student.rank]
        studentrows.append(row)

    # Return a list of all the rows for this table -- the description, header
    # rows, and the rows with the students
    return list(itertools.chain([description, header_row], studentrows))
