import os
import sys
import json

from course import Course
from courserequests import CourseRequests

# Canvas config info

pathname = os.path.dirname(sys.argv[0])
abspath = os.path.abspath(pathname)

config_file_path = '%s/config/config.json' % abspath

# define new CourseRequests object for Canvas API calls.
with open(config_file_path) as config_file:
    canvas_keys = json.loads(config_file.read())

course = Course(canvas_keys['course_name'], canvas_keys['course_id'])
courserequests = CourseRequests(course, canvas_keys['host'],
                    canvas_keys['access_token'])
# end CourseRequests definition
